Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: liburjtag
Source: http://github.com/ahp-electronics/liburjtag
Comment: forked from http://www.urjtag.org
         this file is copied from packagr urjtag, we don't need the data files

Files: *
Copyright: © 2002-2003 ETC s.r.o.
License: GPL-2+

Files: AUTHORS
 MAINTAINERS
 NEWS
 README
 bindings/python/.gitignore
 bindings/python/setup.py.in
 bindings/python/t_srst.py
 bindings/python/t_urjtag_chain.py
Copyright: © 2009-2016 Various authors
License: GPL-2+

Files: Makefile.rules
Copyright: © 2003 ETC s.r.o.
           © 2004 Marcel Telka
License: GPL-2+

Files: src/bsdl/bsdl.c
       src/bsdl/bsdl_msg.h
       src/bsdl/bsdl_parser.h
       src/bsdl/bsdl_sem.c
       src/bsdl/bsdl_sysdep.h
       src/bsdl/bsdl_types.h
       src/bsdl/vhdl_parser.h
       src/bus/fjmem.c
       src/bus/jopcyc.c
       src/bus/zefant-xs3.c
       src/cmd/cmd_bsdl.c
       src/cmd/cmd_svf.c
       src/svf/svf.[ch]
       src/svf/svf_bison.y
       src/tap/cable/ft2232.c
Copyright: © 2004-2008 Arnim Laeuger
License: GPL-2+

Files: src/bus/au1500.c
Copyright: © 2003 BLXCPU co. Ltd.
License: GPL-2+

Files: src/bus/avr32.c
Copyright: © 2008 Gabor Juhos <juhosg@openwrt.org>
License: GPL-2+

Files: src/bus/ejtag.c
Copyright: © 2005 Marek Michalkiewicz
License: GPL-2+

Files: src/bus/generic_bus.[ch]
       src/tap/cable/vision_ep9307.c
Copyright: © 2008 H. Hartley Sweeten <hsweeten@visionengravers.com>
License: GPL-2+

Files: src/bus/h7202.c
Copyright: © 2005 Raphael Mack <mail@raphael-mack.de>
License: GPL-2+

Files: src/bus/lh7a400.c
Copyright: © 2002-2003 Marcel Telka <marcel@telka.sk>
           © 2004 Marko Roessler <marko.roessler@indakom.de>
License: GPL-2+

Files: src/bus/mpc5200.c
       src/bus/mpc824x.c
       src/cmd/cmd_reset.c
       src/cmd/cmd_salias.c
Copyright: © 2003-2004 Marcel Telka <marcel@telka.sk>
License: GPL-2+

Files: src/bus/pxa2x0_mc.h
Copyright: © 2002-2003 ETC s.r.o.
License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the ETC s.r.o. nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 Written by Marcel Telka <marcel@telka.sk>, 2002, 2003.

Files: src/bus/s3c4510x.c
Copyright: © 2003 Jiun-Shian Ho <asky@syncom.com.tw>
           © 2002-2003 Marcel Telka <marcel@telka.sk>
License: GPL-2+

Files: src/bus/sharc21065l.c
       src/flash/amd_flash.c
Copyright: © 2006 Kila Medical Systems
License: GPL-2+

Files: src/bus/slsup3.c
       src/bus/writemem.c
       src/cmd/cmd_writemem.c
Copyright: © 2005 Kent Palmkvist
License: GPL-2+

Files: src/bus/tx4925.c
Copyright: © 2003 RightHand Technologies, Inc.
License: GPL-2+

Files: src/cmd/cmd_debug.c
       src/cmd/cmd_test.c
Copyright: © 2005 Protoparts
License: GPL-2+

Files: src/cmd/cmd_eraseflash.c
Copyright: © 2003 Thomas Froehlich <t.froehlich@gmx.at>
License: GPL-2+

Files: src/flash/amd.c
Copyright: © 2003 August Hörandl <august.hoerandl@gmx.at>
License: GPL-2+

Files: src/jim/intel_28f800b3.c
       src/jim/some_cpu.c
       src/jim/jim_tap.c
Copyright: © 2008 Kolja Waschk <kawk>
License: other
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   .
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   .
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.

Files: include/ansidecl.h
       include/filenames.h
       include/libiberty.h
       src/lib/lrealpath.c
       src/lib/make-relative-prefix.c
Copyright: © 1987-2007 Free Software Foundation, Inc.
License: GPL-2+

Files: src/lib/fclock.c
Copyright: © 2005 Hein Roehrig
License: GPL-2+

Files: src/lib/lbasename.c
       src/lib/safe-ctype.[ch]
Copyright: © 2001-2002 Free Software Foundation, Inc.
License: LGPL-2+
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

Files: src/tap/cable.c
Copyright: © 2003 ETC s.r.o.
           © 2005 Hein Roehrig
           © 2008 Kolja Waschk
License: GPL-2+

Files: src/tap/usbconn.c
       src/tap/cable/jim.c
       src/tap/cable/usbblaster.c
       src/tap/usbconn/libusb.c
Copyright: © 2006-2008 Kolja Waschk
License: GPL-2+

Files: src/tap/cable/wiggler2.c
Copyright: © 2003 Ultra d.o.o.
License: GPL-2+

Files: src/tap/cable/xpc.c
Copyright: © 2008 Kolja Waschk
           © 2002-2003 ETC s.r.o.
License: GPL-2+

Files: src/tap/parport/ppi.c
Copyright: © 2005 Daniel O'Connor
License: GPL-2+

Files: data/analog/bf506/bf506
 data/analog/bf518/bf518
 data/analog/bf527/bf527
 data/analog/bf533/bf533
 data/analog/bf537/bf537
 data/analog/bf538/bf538
 data/analog/bf548/bf548
 data/analog/bf561/bf561
 data/analog/bf592/bf592
 data/analog/bfin/bfin
 data/atmel/at91sam7s256/at91sam7s_tq48v0
 data/atmel/at91sam7s256/at91sam7s_tq64v0
 data/brecis/msp2006/msp2006
 data/bsdl/STD_1149_1_1990
 data/bsdl/STD_1149_1_1994
 data/bsdl/STD_1149_1_2001
 data/bsdl/STD_1532_2001
 data/bsdl/STD_1532_2002
 data/freescale/mpc8377/mpc8377
 data/freescale/mpc8378/mpc8378
 data/freescale/mpc8379/mpc8379
 data/hitachi/ar7300/ar7300
 data/ibm/ppc405ep/STEPPINGS
 data/ibm/ppc405ep/ppc405ep
 data/intel/ixp465/ixp465
 data/lattice/lc4064zc/lc4064zc
 data/lattice/lcmxo2-1200-csbga132/lcmxo2-1200-csbga132
 data/lattice/lfec2-12e/lfec2-12e
 data/xilinx/xc18v02pc44/xc18v02pc44
 data/xilinx/xc18v04pc44/xc18v04pc44
 data/xilinx/xc2c32a-cp56/STEPPINGS
 data/xilinx/xc2c32a-cp56/xc2c32a-cp56
 data/xilinx/xc2c32a-cv64/STEPPINGS
 data/xilinx/xc2c32a-cv64/xc2c32a-cv64
 data/xilinx/xc2c32a-vq44/STEPPINGS
 data/xilinx/xc2c32a-vq44/xc2c32a-vq44
 data/xilinx/xc2c64a-vq44/xc2c64a-vq44
 data/xilinx/xc2s200e-pq208/xc2s200e-pq208
 data/xilinx/xc2v1000-fg256/xc2v1000-fg256
 data/xilinx/xc2v250-fg256/xc2v250-fg256
 data/xilinx/xc2v80-fg256/xc2v80-fg256
 data/xilinx/xc2vp4/STEPPINGS
 data/xilinx/xc2vp4/xc2vp4
 data/xilinx/xc3s1000/STEPPINGS
 data/xilinx/xc3s100e_die/STEPPINGS
 data/xilinx/xc3s100e_die/xc3s100e_die
 data/xilinx/xc3s100e_die/xc3s100e_pq208
 data/xilinx/xc3s100e_die/xc3s100e_tq144
 data/xilinx/xc3s100e_die/xc3s100e_vq100
 data/xilinx/xc3s1200e_fg320/STEPPINGS
 data/xilinx/xc3s1200e_fg320/xc3s1200e_fg320
 data/xilinx/xc3s1400a/STEPPINGS
 data/xilinx/xc3s1400a/xc3s1400a
 data/xilinx/xc3s1400a/xc3s1400a_fg484
 data/xilinx/xc3s1400a/xc3s1400a_fg676
 data/xilinx/xc3s1400a/xc3s1400a_ft256
 data/xilinx/xc3s1500/STEPPINGS
 data/xilinx/xc3s1500/xc3s1500
 data/xilinx/xc3s1500/xc3s1500_fg320
 data/xilinx/xc3s1500/xc3s1500_fg456
 data/xilinx/xc3s1500/xc3s1500_fg676
 data/xilinx/xc3s1500/xc3s1500l
 data/xilinx/xc3s1500/xc3s1500l_fg320
 data/xilinx/xc3s1500/xc3s1500l_fg456
 data/xilinx/xc3s1500/xc3s1500l_fg676
 data/xilinx/xc3s1600e/STEPPINGS
 data/xilinx/xc3s1600e/xc3s1600e
 data/xilinx/xc3s200/STEPPINGS
 data/xilinx/xc3s200/xc3s200
 data/xilinx/xc3s200/xc3s200_ft256
 data/xilinx/xc3s200/xc3s200_pq208
 data/xilinx/xc3s200/xc3s200_tq144
 data/xilinx/xc3s200/xc3s200_vq100
 data/xilinx/xc3s2000/STEPPINGS
 data/xilinx/xc3s2000/xc3s2000
 data/xilinx/xc3s2000/xc3s2000_fg456
 data/xilinx/xc3s2000/xc3s2000_fg676
 data/xilinx/xc3s2000/xc3s2000_fg900
 data/xilinx/xc3s2000/xc3s2000l
 data/xilinx/xc3s2000/xc3s2000l_fg676
 data/xilinx/xc3s2000/xc3s2000l_fg900
 data/xilinx/xc3s200a/STEPPINGS
 data/xilinx/xc3s200a/xc3s200a
 data/xilinx/xc3s200a/xc3s200a_fg320
 data/xilinx/xc3s200a/xc3s200a_ft256
 data/xilinx/xc3s200a/xc3s200a_vq100
 data/xilinx/xc3s400/STEPPINGS
 data/xilinx/xc3s400/xc3s400
 data/xilinx/xc3s400/xc3s400_fg320
 data/xilinx/xc3s400/xc3s400_fg456
 data/xilinx/xc3s400/xc3s400_ft256
 data/xilinx/xc3s400/xc3s400_pq208
 data/xilinx/xc3s400/xc3s400_tq144
 data/xilinx/xc3s4000/STEPPINGS
 data/xilinx/xc3s4000/xc3s4000
 data/xilinx/xc3s4000/xc3s4000_fg1156
 data/xilinx/xc3s4000/xc3s4000_fg676
 data/xilinx/xc3s4000/xc3s4000_fg900
 data/xilinx/xc3s4000/xc3s4000l
 data/xilinx/xc3s4000/xc3s4000l_fg1156
 data/xilinx/xc3s4000/xc3s4000l_fg900
 data/xilinx/xc3s400a/STEPPINGS
 data/xilinx/xc3s400a/xc3s400a
 data/xilinx/xc3s400a/xc3s400a_fg320
 data/xilinx/xc3s400a/xc3s400a_fg400
 data/xilinx/xc3s400a/xc3s400a_ft256
 data/xilinx/xc3s50/STEPPINGS
 data/xilinx/xc3s50/xc3s50
 data/xilinx/xc3s50/xc3s50_cp132
 data/xilinx/xc3s50/xc3s50_pq208
 data/xilinx/xc3s50/xc3s50_tq144
 data/xilinx/xc3s50/xc3s50_vq100
 data/xilinx/xc3s5000/STEPPINGS
 data/xilinx/xc3s5000/xc3s5000
 data/xilinx/xc3s5000/xc3s5000_fg1156
 data/xilinx/xc3s5000/xc3s5000_fg900
 data/xilinx/xc3s500e_fg320/STEPPINGS
 data/xilinx/xc3s500e_fg320/xc3s500e_fg320
 data/xilinx/xc3s500e_fg320/xc3s500e_ft256
 data/xilinx/xc3s500e_fg320/xc3s500e_pq208
 data/xilinx/xc3s50a/STEPPINGS
 data/xilinx/xc3s50a/xc3s50a
 data/xilinx/xc3s50a/xc3s50a_ft256
 data/xilinx/xc3s50a/xc3s50a_tq144
 data/xilinx/xc3s50a/xc3s50a_vq100
 data/xilinx/xc3s700a/STEPPINGS
 data/xilinx/xc3s700a/xc3s700a
 data/xilinx/xc3s700a/xc3s700a_fg400
 data/xilinx/xc3s700a/xc3s700a_fg484
 data/xilinx/xc3s700a/xc3s700a_ft256
 data/xilinx/xc4vfx100/STEPPINGS
 data/xilinx/xc4vfx100/xc4vfx100
 data/xilinx/xc4vfx100/xc4vfx100_ff1152
 data/xilinx/xc4vfx100/xc4vfx100_ff1517
 data/xilinx/xc4vfx12/STEPPINGS
 data/xilinx/xc4vfx12/xc4vfx12
 data/xilinx/xc4vfx12/xc4vfx12_ff668
 data/xilinx/xc4vfx12/xc4vfx12_sf363
 data/xilinx/xc4vfx140/STEPPINGS
 data/xilinx/xc4vfx140/xc4vfx140
 data/xilinx/xc4vfx140/xc4vfx140_ff1517
 data/xilinx/xc4vfx20/STEPPINGS
 data/xilinx/xc4vfx20/xc4vfx20
 data/xilinx/xc4vfx20/xc4vfx20_ff672
 data/xilinx/xc4vfx40/STEPPINGS
 data/xilinx/xc4vfx40/xc4vfx40
 data/xilinx/xc4vfx40/xc4vfx40_ff1152
 data/xilinx/xc4vfx40/xc4vfx40_ff672
 data/xilinx/xc4vfx60/STEPPINGS
 data/xilinx/xc4vfx60/xc4vfx60
 data/xilinx/xc4vfx60/xc4vfx60_ff1152
 data/xilinx/xc4vfx60/xc4vfx60_ff672
 data/xilinx/xc4vlx100/STEPPINGS
 data/xilinx/xc4vlx100/xc4vlx100
 data/xilinx/xc4vlx100/xc4vlx100_ff1148
 data/xilinx/xc4vlx100/xc4vlx100_ff1513
 data/xilinx/xc4vlx15/STEPPINGS
 data/xilinx/xc4vlx15/xc4vlx15
 data/xilinx/xc4vlx15/xc4vlx15_ff668
 data/xilinx/xc4vlx15/xc4vlx15_sf363
 data/xilinx/xc4vlx160/STEPPINGS
 data/xilinx/xc4vlx160/xc4vlx160
 data/xilinx/xc4vlx160/xc4vlx160_ff1148
 data/xilinx/xc4vlx160/xc4vlx160_ff1513
 data/xilinx/xc4vlx200/STEPPINGS
 data/xilinx/xc4vlx200/xc4vlx200
 data/xilinx/xc4vlx200/xc4vlx200_ff1513
 data/xilinx/xc4vlx25/STEPPINGS
 data/xilinx/xc4vlx25/xc4vlx25
 data/xilinx/xc4vlx25/xc4vlx25_ff668
 data/xilinx/xc4vlx25/xc4vlx25_sf363
 data/xilinx/xc4vlx40/STEPPINGS
 data/xilinx/xc4vlx40/xc4vlx40
 data/xilinx/xc4vlx40/xc4vlx40_ff1148
 data/xilinx/xc4vlx40/xc4vlx40_ff668
 data/xilinx/xc4vlx60/STEPPINGS
 data/xilinx/xc4vlx60/xc4vlx60
 data/xilinx/xc4vlx60/xc4vlx60_ff1148
 data/xilinx/xc4vlx60/xc4vlx60_ff668
 data/xilinx/xc4vlx80/STEPPINGS
 data/xilinx/xc4vlx80/xc4vlx80
 data/xilinx/xc4vlx80/xc4vlx80_ff1148
 data/xilinx/xc4vsx25/STEPPINGS
 data/xilinx/xc4vsx25/xc4vsx25
 data/xilinx/xc4vsx25/xc4vsx25_ff668
 data/xilinx/xc4vsx35/STEPPINGS
 data/xilinx/xc4vsx35/xc4vsx35
 data/xilinx/xc4vsx35/xc4vsx35_ff668
 data/xilinx/xc4vsx55/STEPPINGS
 data/xilinx/xc4vsx55/xc4vsx55
 data/xilinx/xc4vsx55/xc4vsx55_ff1148
 data/xilinx/xc6slx100/STEPPINGS
 data/xilinx/xc6slx100/xc6slx100
 data/xilinx/xc6slx100/xc6slx100_csg484
 data/xilinx/xc6slx100/xc6slx100_fgg484
 data/xilinx/xc6slx100/xc6slx100_fgg676
 data/xilinx/xc6slx100/xc6slx100l
 data/xilinx/xc6slx100/xc6slx100l_csg484
 data/xilinx/xc6slx100/xc6slx100l_fgg484
 data/xilinx/xc6slx100/xc6slx100l_fgg676
 data/xilinx/xc6slx100/xc6slx100t_csg484
 data/xilinx/xc6slx100/xc6slx100t_fgg484
 data/xilinx/xc6slx100/xc6slx100t_fgg676
 data/xilinx/xc6slx100/xc6slx100t_fgg900
 data/xilinx/xc6slx100t/STEPPINGS
 data/xilinx/xc6slx100t/xc6slx100t
 data/xilinx/xc6slx150/STEPPINGS
 data/xilinx/xc6slx150/xc6slx150
 data/xilinx/xc6slx150/xc6slx150_csg484
 data/xilinx/xc6slx150/xc6slx150_fgg484
 data/xilinx/xc6slx150/xc6slx150_fgg676
 data/xilinx/xc6slx150/xc6slx150_fgg900
 data/xilinx/xc6slx150/xc6slx150l
 data/xilinx/xc6slx150/xc6slx150l_csg484
 data/xilinx/xc6slx150/xc6slx150l_fgg484
 data/xilinx/xc6slx150/xc6slx150l_fgg676
 data/xilinx/xc6slx150/xc6slx150l_fgg900
 data/xilinx/xc6slx150/xc6slx150t_csg484
 data/xilinx/xc6slx150/xc6slx150t_fgg484
 data/xilinx/xc6slx150/xc6slx150t_fgg676
 data/xilinx/xc6slx150/xc6slx150t_fgg900
 data/xilinx/xc6slx150t/STEPPINGS
 data/xilinx/xc6slx150t/xc6slx150t
 data/xilinx/xc6slx16/STEPPINGS
 data/xilinx/xc6slx16/xa6slx16
 data/xilinx/xc6slx16/xa6slx16_csg225
 data/xilinx/xc6slx16/xa6slx16_csg324
 data/xilinx/xc6slx16/xa6slx16_ftg256
 data/xilinx/xc6slx16/xc6slx16
 data/xilinx/xc6slx16/xc6slx16_cpg196
 data/xilinx/xc6slx16/xc6slx16_csg225
 data/xilinx/xc6slx16/xc6slx16_csg324
 data/xilinx/xc6slx16/xc6slx16_ftg256
 data/xilinx/xc6slx16/xc6slx16l
 data/xilinx/xc6slx16/xc6slx16l_cpg196
 data/xilinx/xc6slx16/xc6slx16l_csg225
 data/xilinx/xc6slx16/xc6slx16l_csg324
 data/xilinx/xc6slx16/xc6slx16l_ftg256
 data/xilinx/xc6slx25/STEPPINGS
 data/xilinx/xc6slx25/xa6slx25
 data/xilinx/xc6slx25/xa6slx25_csg324
 data/xilinx/xc6slx25/xa6slx25_fgg484
 data/xilinx/xc6slx25/xa6slx25_ftg256
 data/xilinx/xc6slx25/xa6slx25t_csg324
 data/xilinx/xc6slx25/xa6slx25t_fgg484
 data/xilinx/xc6slx25/xc6slx25
 data/xilinx/xc6slx25/xc6slx25_csg324
 data/xilinx/xc6slx25/xc6slx25_fgg484
 data/xilinx/xc6slx25/xc6slx25_ftg256
 data/xilinx/xc6slx25/xc6slx25l
 data/xilinx/xc6slx25/xc6slx25l_csg324
 data/xilinx/xc6slx25/xc6slx25l_fgg484
 data/xilinx/xc6slx25/xc6slx25l_ftg256
 data/xilinx/xc6slx25/xc6slx25t_csg324
 data/xilinx/xc6slx25/xc6slx25t_fgg484
 data/xilinx/xc6slx25t/STEPPINGS
 data/xilinx/xc6slx25t/xa6slx25t
 data/xilinx/xc6slx25t/xc6slx25t
 data/xilinx/xc6slx4/STEPPINGS
 data/xilinx/xc6slx4/xa6slx4
 data/xilinx/xc6slx4/xa6slx4_csg225
 data/xilinx/xc6slx4/xc6slx4
 data/xilinx/xc6slx4/xc6slx4_cpg196
 data/xilinx/xc6slx4/xc6slx4_csg225
 data/xilinx/xc6slx4/xc6slx4_tqg144
 data/xilinx/xc6slx4/xc6slx4l
 data/xilinx/xc6slx4/xc6slx4l_cpg196
 data/xilinx/xc6slx4/xc6slx4l_csg225
 data/xilinx/xc6slx4/xc6slx4l_tqg144
 data/xilinx/xc6slx45/STEPPINGS
 data/xilinx/xc6slx45/xa6slx45
 data/xilinx/xc6slx45/xa6slx45_csg324
 data/xilinx/xc6slx45/xa6slx45_fgg484
 data/xilinx/xc6slx45/xc6slx45
 data/xilinx/xc6slx45/xc6slx45_csg324
 data/xilinx/xc6slx45/xc6slx45_csg484
 data/xilinx/xc6slx45/xc6slx45_fgg484
 data/xilinx/xc6slx45/xc6slx45_fgg676
 data/xilinx/xc6slx45/xc6slx45l
 data/xilinx/xc6slx45/xc6slx45l_csg324
 data/xilinx/xc6slx45/xc6slx45l_csg484
 data/xilinx/xc6slx45/xc6slx45l_fgg484
 data/xilinx/xc6slx45/xc6slx45l_fgg676
 data/xilinx/xc6slx45t/STEPPINGS
 data/xilinx/xc6slx45t/xa6slx45t
 data/xilinx/xc6slx45t/xa6slx45t_csg324
 data/xilinx/xc6slx45t/xa6slx45t_fgg484
 data/xilinx/xc6slx45t/xc6slx45t
 data/xilinx/xc6slx45t/xc6slx45t_csg324
 data/xilinx/xc6slx45t/xc6slx45t_csg484
 data/xilinx/xc6slx45t/xc6slx45t_fgg484
 data/xilinx/xc6slx75/STEPPINGS
 data/xilinx/xc6slx75/xa6slx75
 data/xilinx/xc6slx75/xa6slx75_fgg484
 data/xilinx/xc6slx75/xa6slx75t_fgg484
 data/xilinx/xc6slx75/xc6slx75
 data/xilinx/xc6slx75/xc6slx75_csg484
 data/xilinx/xc6slx75/xc6slx75_fgg484
 data/xilinx/xc6slx75/xc6slx75_fgg676
 data/xilinx/xc6slx75/xc6slx75l
 data/xilinx/xc6slx75/xc6slx75l_csg484
 data/xilinx/xc6slx75/xc6slx75l_fgg484
 data/xilinx/xc6slx75/xc6slx75l_fgg676
 data/xilinx/xc6slx75/xc6slx75t_csg484
 data/xilinx/xc6slx75/xc6slx75t_fgg484
 data/xilinx/xc6slx75/xc6slx75t_fgg676
 data/xilinx/xc6slx75t/STEPPINGS
 data/xilinx/xc6slx75t/xa6slx75t
 data/xilinx/xc6slx75t/xc6slx75t
 data/xilinx/xc6slx9/STEPPINGS
 data/xilinx/xc6slx9/xa6slx9
 data/xilinx/xc6slx9/xa6slx9_csg225
 data/xilinx/xc6slx9/xa6slx9_csg324
 data/xilinx/xc6slx9/xa6slx9_ftg256
 data/xilinx/xc6slx9/xc6slx9
 data/xilinx/xc6slx9/xc6slx9_cpg196
 data/xilinx/xc6slx9/xc6slx9_csg225
 data/xilinx/xc6slx9/xc6slx9_csg324
 data/xilinx/xc6slx9/xc6slx9_ftg256
 data/xilinx/xc6slx9/xc6slx9_tqg144
 data/xilinx/xc6slx9/xc6slx9l
 data/xilinx/xc6slx9/xc6slx9l_cpg196
 data/xilinx/xc6slx9/xc6slx9l_csg225
 data/xilinx/xc6slx9/xc6slx9l_csg324
 data/xilinx/xc6slx9/xc6slx9l_ftg256
 data/xilinx/xc6slx9/xc6slx9l_tqg144
 data/xilinx/xc7a100t/STEPPINGS
 data/xilinx/xc7a100t/xc7a100t-csg324
 data/xilinx/xc95108/STEPPINGS
 data/xilinx/xc95108/xc95108
 data/xilinx/xc95108/xc95108_pc84
 data/xilinx/xc95108/xc95108_pq100
 data/xilinx/xc95108/xc95108_pq160
 data/xilinx/xc95108/xc95108_tq100
 data/xilinx/xc95144xl/STEPPINGS
 data/xilinx/xc95144xl/xc95144xl_cs144
 data/xilinx/xc95144xl/xc95144xl_tq100
 data/xilinx/xc95144xl/xc95144xl_tq144
 data/xilinx/xc95288xl/STEPPINGS
 data/xilinx/xc95288xl/xc95288xl
 data/xilinx/xc95288xl/xc95288xl_bg256
 data/xilinx/xc95288xl/xc95288xl_cs280
 data/xilinx/xc95288xl/xc95288xl_fg256
 data/xilinx/xc95288xl/xc95288xl_pq208
 data/xilinx/xc95288xl/xc95288xl_tq144
 data/xilinx/xc9536xl/STEPPINGS
 data/xilinx/xc9536xl/xc9536xl_cs48
 data/xilinx/xc9536xl/xc9536xl_pc44
 data/xilinx/xc9536xl/xc9536xl_vq44
 data/xilinx/xc9536xl/xc9536xl_vq64
 data/xilinx/xc9572xl/STEPPINGS
 data/xilinx/xc9572xl/xc9572xl_cs48
 data/xilinx/xc9572xl/xc9572xl_pc44
 data/xilinx/xc9572xl/xc9572xl_tq100
 data/xilinx/xc9572xl/xc9572xl_vq44
 data/xilinx/xc9572xl/xc9572xl_vq64
 data/xilinx/xccace/STEPPINGS
 data/xilinx/xccace/xccace_tq144
 data/xilinx/xcf01s/STEPPINGS
 data/xilinx/xcf01s/xcf01s
 data/xilinx/xcf01s/xcf01s_vo20
 data/xilinx/xcf02s/STEPPINGS
 data/xilinx/xcf02s/xcf02s
 data/xilinx/xcf02s/xcf02s_vo20
 data/xilinx/xcf04s/STEPPINGS
 data/xilinx/xcf04s/xcf04s
 data/xilinx/xcf04s/xcf04s_vo20
 data/xilinx/xcf08p/STEPPINGS
 data/xilinx/xcf08p/xcf08p
 data/xilinx/xcf08p/xcf08p_fs48
 data/xilinx/xcf08p/xcf08p_vo48
 data/xilinx/xcf16p/STEPPINGS
 data/xilinx/xcf16p/xcf16p
 data/xilinx/xcf16p/xcf16p_fs48
 data/xilinx/xcf16p/xcf16p_vo48
 data/xilinx/xcf32p/STEPPINGS
 data/xilinx/xcf32p/xcf32p
 data/xilinx/xcf32p/xcf32p_fs48
 data/xilinx/xcf32p/xcf32p_vo48
Copyright: © 2009-2016 Various authors
License: GPL-2+

Files: debian/*
Copyright: © 2008-2011 Uwe Hermann <uwe@debian.org>
           © 2016 Geert Stappers <stappers@debian.org>
           © 2018, Ilia Platone <info@iliaplatone.com>
           © 2023, Thorsten Alteholz <debian@alteholz.de>
License: PD
 The packaging done by Uwe Hermann <uwe@debian.org> is hereby
 released as public domain.
 .
 The parts contributed by Ilia Platone are licensed under LGPL3+

#License: LGPL-3+
# On Debian systems, the complete text of the GNU Lesser General
# Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: GPL-2+
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

# initial copyright information
#
# This package was debianized by:
#
#     Uwe Hermann <uwe@debian.org> on Wed, 19 Nov 2008 13:18:38 +0100
#
# It was downloaded from:
#
#     http://www.urjtag.org
#
# Upstream Authors:
#
#     Marcel Telka <marcel@telka.sk>
#     Arnim Läuger <arniml@users.sourceforge.net>
#     Kolja Waschk <urjtag@ixo.de>
#     and many, many others (see AUTHORS)
#

